package and.mc.cs.ut.ee.awsstudentdemo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import and.mc.cs.ut.ee.awsadaptorlibrary.CloudServiceCommunicator;

public class MainActivity extends AppCompatActivity {
    private static final int SELECT_PHOTO = 100;
    private static final String TAG = MainActivity.class.getSimpleName();

    private CloudServiceCommunicator cloudCommunicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void doWorkOnCloud(View v) throws IOException {
        cloudCommunicator = new CloudServiceCommunicator(
                "x.x.x.x", //host ip address
                "xxxxx", //username (your student code)
                "xxxxx", //password (your student code)
                22
        );

        cloudCommunicator.addListener(new CloudServiceCommunicator.SSHListener() {
            @Override
            public void commandResultReceived(String result) {
                Log.i(TAG, result);
            }

            @Override
            public void fileSent() {
                Log.i(TAG, "File sent");
            }
        });

        // Send a simple ECHO command just to see that we really are connected.
        String command = "echo --- CONNECTED TO: $(hostname)---;";
        cloudCommunicator.doSendCommandTask(command);

        //// Intent to choose photo
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }


    /**
     * Here we define the behaviour for sending the selected photo to the instance
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    new ProcessImageTask().execute(selectedImage);
                }
        }
    }


    private class ProcessImageTask extends AsyncTask<Uri , String, Uri> {
        protected Uri doInBackground(Uri... imageuris) {
            String filename = Utils.getFileNameByUri(MainActivity.this, imageuris[0]);

            // 1. Send the image
            try {
                publishProgress("Sending image..");
                InputStream imageStream = getContentResolver().openInputStream(imageuris[0]);
                cloudCommunicator.sendFile(imageStream, filename);
            } catch (FileNotFoundException e) {
                publishProgress(e.getMessage());
            }

            // 2. Apply command to the image remotely.
            publishProgress("Processing image..");
            String outputFilename = "glowing_" + filename;
            String command = String.format("gmic -input %s -glow 10%% -output %s", filename, outputFilename);
            cloudCommunicator.sendCommand(command);

            // 3. Retrieve the processed output image from remote
            publishProgress("Retrieving image..");
            cloudCommunicator.receiveFile(outputFilename, getExternalFilesDir(null));

            return Uri.fromFile(new File(getExternalFilesDir(null),outputFilename));
        }

        protected void onProgressUpdate(String... progress) {
            Toast.makeText(getApplicationContext(), progress[0], Toast.LENGTH_SHORT).show();
        }

        protected void onPostExecute(Uri result) {
            String filepath = getExternalFilesDir(null).getAbsolutePath() +
                    Utils.getFileNameByUri(getApplicationContext(), result);
            Toast.makeText(getApplicationContext(), "DONE! file stored to: "+filepath, Toast.LENGTH_LONG).show();
            showImage(result);
        }
    }



    /** Opens a pop-up dialog showing the image corresponding to the provided Uri */
    public void showImage(Uri imageUri) {
        Dialog builder = new Dialog(this);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);

        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        ImageView imageView = new ImageView(this);
        Bitmap scaled;
        //Scale down the bitmap to avoid bitmap memory errors, set it to the imageview.
        //This code scales it such that the width is 1280
        try {
            Bitmap bmp = MediaStore.Images.Media.getBitmap(this.getContentResolver(),imageUri);
            int nh = (int) ( bmp.getHeight() * (1280.0 / bmp.getWidth()) );
            scaled = Bitmap.createScaledBitmap(bmp, 1280, nh, true);
            imageView.setImageBitmap(scaled);
        } catch (IOException e) {
            e.printStackTrace();
        }

        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
}




