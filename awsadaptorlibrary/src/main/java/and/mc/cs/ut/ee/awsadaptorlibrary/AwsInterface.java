package and.mc.cs.ut.ee.awsadaptorlibrary;

public interface AwsInterface {
    void showError(String msg);
    void displayMessage(String msg);
}
