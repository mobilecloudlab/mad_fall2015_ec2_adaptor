package and.mc.cs.ut.ee.awsadaptorlibrary.scp;

import android.util.Log;

import com.jcraft.jsch.Logger;

/**
 * Created by Jakob on 13.02.2015.
 *
 * Allows to receive jsch log messages in android. Disabled by default.
 */
public class AndroidScpLogger implements Logger {
    private static final String TAG = AndroidScpLogger.class.getName();



    private boolean enabled = false;

    public AndroidScpLogger() {
    }

    @Override
    public boolean isEnabled(int level) {
        return enabled;
    }

    @Override
    public void log(int level, String message) {
        Log.v(TAG, message);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
