package and.mc.cs.ut.ee.awsadaptorlibrary.aws;

import com.amazonaws.services.ec2.model.Instance;

/**
 * Created by Jakob on 14.11.2014.
 */
public interface AwsInstanceInterface {
    void onInstanceUpdate(Instance i, int code);
}
