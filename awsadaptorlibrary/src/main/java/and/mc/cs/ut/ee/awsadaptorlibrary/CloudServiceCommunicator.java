package and.mc.cs.ut.ee.awsadaptorlibrary;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import and.mc.cs.ut.ee.awsadaptorlibrary.scp.ScpManager;

/**
 * Created by Jakob on 19.10.2015.
 *
 * This class provides means for sending commands via SSH and transmitting
 * files via SCP. Key-based authentication and passphrase based authentication are supported.
 */
public class CloudServiceCommunicator {

    public static final String TAG = CloudServiceCommunicator.class.getName();

    private ScpManager scpManager;

    /** Interface for defining events based on command results and file transfer finishing */
    public interface SSHListener{
        void commandResultReceived(String s);
        void fileSent();
    }
    private List<SSHListener> listeners;

    /**
     * For authentication using a key file
     * @param host
     * @param username ssh username
     * @param port ssh port (22 ? )
     * @param keyFileInputStream     */
    public CloudServiceCommunicator(String host, String username, int port, InputStream keyFileInputStream) {
        this.listeners = new ArrayList<>();

        scpManager = new ScpManager();
        scpManager.configureSession(username,
                host,
                port,
                keyFileInputStream);
    }

    /**
     * For authentication using a passphrase
     * @param host
     * @param username ssh username
     * @param password
     * @param port ssh port (22 ? )     */
    public CloudServiceCommunicator(String host, String username, String password, int port) {
        this.listeners = new ArrayList<>();

        scpManager = new ScpManager();
        scpManager.configureSession(username,
                password,
                host,
                port
        );
    }


    /** Sends file using a separate AsyncTask. So this
     * method can be called on the UI thread without trouble
     *
     * @param imageStream
     * @param fileName
     */
    public void doSendFileTask(InputStream imageStream, String fileName) {
        new SendFileTask(imageStream).execute(fileName);
    }

    /** Sends file to the remote instance configured in this
     * CloudServiceCommunicator. The file is sent to whichever
     * directory the user is currently in.
     *
     * @param fileIS
     * @param fileName the name for the file to be written to the remote
     */
    public void sendFile(InputStream fileIS, String fileName) {
            try {
                scpManager.sendInputStreamAsFile(fileIS, fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     * Reads a given file from the remote. The file which is
     * @param fileToReceive the file which will be transferred from the remote
     * @param pathToSave the path the file will be saved to
     */
    public void receiveFile(String fileToReceive, File pathToSave) {
        scpManager.receiveFile(fileToReceive, pathToSave);
    }



    public String sendCommand(String s) {
        String result = "";
        Log.i(TAG, "***SSHCommandTask STARTED");
        if (scpManager != null) {
            String command = s;
            result = scpManager.sendCommand(command);
            Log.d(TAG, "*** SSHCommandTask Finished");
        } else {
            Log.e(TAG, "ERROR - tried doing SCP with host null!");
        }
        Log.d(TAG, "*** RESULT = " + result);
        return result;
    }

    public void doSendCommandTask(String s){
        new SSHCommandTask().execute(s);
    }

    public class SSHCommandTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            return sendCommand(params[0]);
        }
        @Override
        protected void onPostExecute(String string) {
            //Notify listeners
            for (SSHListener listener : listeners){
                listener.commandResultReceived(string);
            }
        }
    }

    private class SendFileTask extends AsyncTask<String, Void, Void> {
        InputStream fileIS;
        public SendFileTask(InputStream fileIS) {
            this.fileIS = fileIS;
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.i(TAG, "***SendFileTask STARTED");
            sendFile(fileIS,params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            Log.d(TAG, "*** SendFileTask Finished");
            //Notify listeners
            for (SSHListener listener : listeners){
                listener.fileSent();
            }
        }
    }

    public void addListener(SSHListener toAdd) {
        listeners.add(toAdd);
    }
}
