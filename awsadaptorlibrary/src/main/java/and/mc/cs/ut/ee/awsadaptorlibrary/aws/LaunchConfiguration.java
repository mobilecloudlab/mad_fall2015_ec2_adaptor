package and.mc.cs.ut.ee.awsadaptorlibrary.aws;

/** A convenience class which hold the necessary values for launching an instance
 * such as instance type, AMI id, ..
 */
public class LaunchConfiguration {

    String instanceType;
    String imageId;
    String keyName;
    String securityGroupId;
    String subnetId;
    String availabilityZone;

    /**
     * A wrapper for the configuration used to launch an instance. This version of the
     * constructor has the default value of "subnet-5da54504" for the subnet and
     * "us-east-1c" for the availability zone.
     *
     * @param instanceType e.g. "t2.medium"
     * @param imageId the AMI id
     * @param keyName the keyfile used
     * @param securityGroup security group associated with the instance
     */
    public LaunchConfiguration(String instanceType, String imageId,
                               String keyName, String securityGroup) {
        this.instanceType = instanceType;
        this.imageId = imageId;
        this.keyName = keyName;
        this.securityGroupId = securityGroup;

        //Default values
        this.subnetId = "subnet-5da54504";
        this.availabilityZone = "us-east-1c";
    }


    /**
     * @param instanceType e.g. "t2.medium"
     * @param imageId the AMI id
     * @param keyName the keyfile used
     * @param securityGroup security group associated with the instance
     * @param subnetId e.g. "subnet-5da54504"
     * @param availabilityZone e.g. "us-east-1c"
     */
    public LaunchConfiguration(String instanceType, String imageId, String keyName,
                               String securityGroup, String subnetId, String availabilityZone) {
        this.instanceType = instanceType;
        this.imageId = imageId;
        this.keyName = keyName;
        this.securityGroupId = securityGroup;

        //Default values
        this.subnetId = subnetId;
        this.availabilityZone = availabilityZone;
    }



    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public void setSecurityGroup(String securityGroup) {
        this.securityGroupId = securityGroup;
    }

}
