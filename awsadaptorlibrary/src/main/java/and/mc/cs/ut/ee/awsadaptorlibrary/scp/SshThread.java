package and.mc.cs.ut.ee.awsadaptorlibrary.scp;

import android.app.Activity;
import android.util.Log;

/**
 * Created by Jakob on 2.10.2014.
 */
public class SshThread extends Thread {

    private final String TAG = SshThread.class.getSimpleName();

    private String keyFileName;
    private Activity activity;

    private String command;


    private String host;
    private String username;
    private int port = -1;


    /**
     * In addition to this initializing with this constructor, make sure to provide the
     * username, host address and port aswell.
     *
     * @param activity
     * @param keyFileName the asset .pem key file used to create SSH connection
     */
    public SshThread(Activity activity, String keyFileName) {
        this.keyFileName = keyFileName;
        this.activity = activity;
    }

    @Override
    public void run() {

        SshClient sshClient = new SshClient(activity, keyFileName );
        if (host != null && username != null && port != -1 && command != null){
            try {
                sshClient.connectAndRunCommand("ubuntu", "54.68.46.234", 22, "whoami;hostname");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "Some parameter is missing. Make sure you have set the username, host, port and command.");
        }
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPort(int port) {
        this.port = port;
    }
    public void setCommand(String command) {
        this.command = command;
    }

}
